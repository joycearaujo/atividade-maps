package com.example.joyce.atividademaps;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MainActivity extends FragmentActivity implements GoogleMap.OnMapClickListener {

    SupportMapFragment mapFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        Intent intent = getIntent();
        final int escolha = intent.getIntExtra("escolhi", 1);
        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                iniciarMapa(googleMap, escolha);
            }
        });
    }

    public void iniciarMapa(GoogleMap map, int escolha){

        if(escolha == 1){
            map.setMapType(GoogleMap.MAP_TYPE_NONE);
        }
        else if(escolha == 2){
            map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        }
        else if(escolha == 3){
            map.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
        }
        else if(escolha == 4){
            map.setMapType(GoogleMap.MAP_TYPE_HYBRID);
        }
        else if(escolha == 5){
            map.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
        }

        LatLng fafica = new LatLng(-8.298635, -35.974063);
        LatLng minhaCasa = new LatLng(-8.295364, -35.979166);
        CameraUpdate update = CameraUpdateFactory.newLatLngZoom(minhaCasa,20);
        map.animateCamera(update);
        adicionarMarcadores(map, minhaCasa);

    }

    private void adicionarMarcadores(GoogleMap map, LatLng latLng){
        MarkerOptions markerOptions = new MarkerOptions();
        BitmapDescriptor iconizineo = BitmapDescriptorFactory.fromResource(R.drawable.uni);
        markerOptions.icon(iconizineo);
        markerOptions.position(latLng).title("Minha casa").snippet("Mansão Joyce");
        map.addMarker(markerOptions);
    }

    @Override
    public void onMapClick(LatLng latLng) {

    }
}
